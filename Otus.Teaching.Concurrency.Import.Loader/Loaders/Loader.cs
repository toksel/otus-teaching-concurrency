﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Loader
    {
        Stopwatch _stopwatch = new Stopwatch();
        AppConfiguration _configuration;

        public Loader(AppConfiguration configuartion)
        {
            _configuration = configuartion;
        }

        public void LoadData(int retryNumber = 0)
        {
            try
            {
                var customers = GetAllCustomersFromFile(_configuration.FilePath);
                switch (_configuration.LoaderMode)
                {
                    case LoaderMode.Single:
                        LoadDataChunk(customers);
                        break;
                    case LoaderMode.Separete:
                        LoadDataBySepareteThreads(customers, _configuration.ThreadsCount);
                        break;
                    case LoaderMode.ThreadPool:
                        LoadDataByThreadPool(customers, _configuration.ThreadsCount);
                        break;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.InnerException.Message);
                Console.WriteLine($"Retries left: {_configuration.RetryCount - retryNumber}");
                if (retryNumber < _configuration.RetryCount)
                {
                    Console.WriteLine("Retrying...");
                    LoadData(++retryNumber);
                }
            }
        }

        void LoadDataSync(IList<Customer> customers, int threadId, int retryNumber = 0)
        {
            try
            {
                using var dbContext = new AppDbContext(_configuration);
                dbContext.AddCustomers(customers);
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Thread {threadId} exception: {ex.Message}");
                Console.WriteLine($"Thread {threadId} inner exception: {ex.InnerException.Message}");
                Console.WriteLine($"Thread {threadId} retries left: {_configuration.RetryCount - retryNumber}");
                if (retryNumber < _configuration.RetryCount)
                {
                    Console.WriteLine($"Thread {threadId} is retrying...");
                    LoadDataSync(customers, threadId, ++retryNumber);
                }
            }
        }

        void LoadDataBySepareteThreads(IList<Customer> customers, int threadsCount)
        {
            Console.WriteLine($"Start uploading data to database using separete threads");
            _stopwatch.Restart();

            int cnt = customers.Count / threadsCount;
            var events = new List<EventWaitHandle>();
            for (int i = 0; i < threadsCount; i++)
            {
                IList<Customer> chunk = customers.Where(w => w.Id > cnt * i && w.Id <= cnt * (i + 1)).ToList();
                var threadFinished = new EventWaitHandle(false, EventResetMode.ManualReset);
                events.Add(threadFinished);
                new Thread(() => { LoadDataChunk(chunk); threadFinished.Set(); }).Start();
            }

            WaitHandle.WaitAll(events.ToArray());
            _stopwatch.Stop();
            Console.WriteLine($"Finished uploading data in {_stopwatch.ElapsedMilliseconds} ms.");
        }

        void LoadDataByThreadPool(IList<Customer> customers, int threadsCount)
        {
            Console.WriteLine($"Start uploading data to database using threadpool");
            _stopwatch.Restart();

            int cnt = customers.Count / threadsCount;
            var events = new List<EventWaitHandle>();
            for (int i = 0; i < threadsCount; i++)
            {
                IList<Customer> chunk = customers.Where(w => w.Id > cnt * i && w.Id <= cnt * (i + 1)).ToList();
                var threadFinished = new EventWaitHandle(false, EventResetMode.ManualReset);
                events.Add(threadFinished);
                ThreadPool.QueueUserWorkItem((i) => { LoadDataChunk(chunk); threadFinished.Set(); });
            }

            WaitHandle.WaitAll(events.ToArray());
            _stopwatch.Stop();
            Console.WriteLine($"Finished uploading data in {_stopwatch.ElapsedMilliseconds} ms.");
        }

        void LoadDataChunk(IList<Customer> customers)
        {
            Stopwatch sw = new Stopwatch();
            int theadId = Thread.CurrentThread.ManagedThreadId;
            sw.Start();
            Console.WriteLine($"Load data by thread {theadId}");
            LoadDataSync(customers, theadId);
            sw.Stop();
            Console.WriteLine($"Load data finished by thread {theadId} ({sw.ElapsedMilliseconds} ms)");
        }

        IList<Customer> GetAllCustomersFromFile(string path)
        {
            Console.WriteLine($"Start serializing data from {path}");
            _stopwatch.Restart();

            var factory = new ParserFactory();
            var parser = factory.GetParser(Path.GetExtension(path).ToLower()[1..]);
            var ret = parser.Parse(path);

            _stopwatch.Stop();
            Console.WriteLine($"Finished serializing data in {_stopwatch.ElapsedMilliseconds} ms.");

            return ret;
        }

    }
}
