﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Diagnostics;
using GeneratorProgram = Otus.Teaching.Concurrency.Import.DataGenerator.App.Program;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class GeneratorManager
    {
        AppConfiguration _configuration;
        public GeneratorManager(AppConfiguration configuartion)
        {
            _configuration = configuartion;
        }
        public void Generate()
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Console.WriteLine($"Genarator started with process Id {Process.GetCurrentProcess().Id}...");
            Console.WriteLine($"Writing data to file {_configuration.FilePath}...");
            switch (_configuration.GenerateMode)
            {
                case GenerateMode.Method:
                    GenerateByMethod(_configuration.FilePath, _configuration.EntitiesCount);
                    break;
                case GenerateMode.Process:
                    GenerateByProcess(_configuration.FilePath, _configuration.EntitiesCount);
                    break;
            }
            stopwatch.Stop();
            Console.WriteLine($"Finished in {stopwatch.ElapsedMilliseconds} ms.");
        }
        void GenerateByProcess(string filePath, int count)
        {
            Process process = new Process();
            process.StartInfo.FileName = "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
            process.StartInfo.Arguments = $"{filePath} {count}";
            process.Start();
            process.WaitForExit();
        }
        void GenerateByMethod(string filePath, int count)
            => GeneratorProgram.Main(new string[2] { filePath, count.ToString() });
    }
}
