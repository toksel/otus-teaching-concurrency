﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using System;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder().AddJsonFile("config.json", false).Build();
            var settings = builder.GetSection("AppSettings").Get<AppConfiguration>();
            Console.WriteLine(settings);
            new GeneratorManager(settings).Generate();
            new Loader(settings).LoadData();
        }
    }
}