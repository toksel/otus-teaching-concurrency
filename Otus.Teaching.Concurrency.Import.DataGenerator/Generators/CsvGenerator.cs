﻿using Otus.Teaching.Concurrency.Import.Core.Data;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using var stream = File.Create(_fileName);
            var serializer = new CsvSerializer<Customer>();
            serializer.Serialize(stream, customers);
        }
    }
}
