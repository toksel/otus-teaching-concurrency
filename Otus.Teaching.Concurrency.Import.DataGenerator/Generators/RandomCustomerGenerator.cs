using Bogus;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public static class RandomCustomerGenerator
    {
        public static List<Customer> Generate(int dataCount, bool includeId = true)
        {
            var customers = new List<Customer>();
            var customersFaker = CreateFaker(includeId);

            foreach (var customer in customersFaker.GenerateForever())
            {
                customers.Add(customer);

                if (dataCount == customers.Count)
                    return customers;
            }
            return customers;
        }

        public static Customer GenerateOne() => Generate(1, false).First();

        private static Faker<Customer> CreateFaker(bool includeId)
        {
            var id = 1;
            var customersFaker = new Faker<Customer>()
                .CustomInstantiator(f => new Customer()
                {
                    Id = includeId ? id++ : default
                })
                .RuleFor(u => u.FullName, (f, u) => f.Name.FullName())
                .RuleFor(u => u.Email, (f, u) => f.Internet.Email(u.FullName))
                .RuleFor(u => u.Phone, (f, u) => f.Phone.PhoneNumber("1-###-###-####"));

            return customersFaker;
        }
    }
}