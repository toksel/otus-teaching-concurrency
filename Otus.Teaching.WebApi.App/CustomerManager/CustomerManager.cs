﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.WebApi.App
{
    public class CustomerManager
    {
        private readonly IClient _client;
        private readonly IInputValidator<int> _validator;
        public CustomerManager(IClient client, IInputValidator<int> validator)
        {
            _client = client;
            _validator = validator;
            _validator.OnInvalidInput += Start;
        }

        public void Start()
        {
            while (true)
            {
                Console.WriteLine("1 - get customer by Id, 2 - add new generated customer, 0 - exit");
                var input = Console.ReadLine();
                switch (_validator.Parse(input))
                {
                    case 1:
                        GetCustomerByid();
                        break;
                    case 2:
                        AddRandomCustomer();
                        break;
                    case 0:
                        Exit();
                        break;
                    default:
                        continue;
                }
            }

        }

        async Task GetCustomerByid()
        {
            Console.WriteLine("Enter customer id:");
            var input = Console.ReadLine();
            int customerId = _validator.Parse(input);
            var response = await _client.GetCustomerByIdAsync(customerId);
            Console.WriteLine(response.ToString());
        }

        async Task AddRandomCustomer()
        {
            Customer customer = RandomCustomerGenerator.GenerateOne();
            Console.WriteLine($"Cusomter [{customer}] is being added to the database...");
            var response = await _client.AddCustomerAsync(customer);
            Console.WriteLine(response);
        }

        void Exit()
        {
            _validator.OnInvalidInput -= Start;
            Environment.Exit(0);
        }

    }
}
