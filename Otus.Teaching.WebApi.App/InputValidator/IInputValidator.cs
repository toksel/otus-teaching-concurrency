﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.WebApi.App
{
    public interface IInputValidator<T>
    {
        public event Action OnInvalidInput;
        public T Parse(string input);
    }
}
