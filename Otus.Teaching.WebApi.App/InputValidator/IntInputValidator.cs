﻿using System;
using System.Threading.Tasks;

namespace Otus.Teaching.WebApi.App
{
    public class IntInputValidator : IInputValidator<int>
    {
        public event Action OnInvalidInput;

        public int Parse(string input)
        {
            if (!int.TryParse(input, out int result))
            {
                OnInvalidInput?.Invoke();
            }
            return result;
        }
    }
}
