﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.WebApi.App
{
    internal class ApiClient : IClient
    {
        private readonly string _url;
        private readonly HttpClient _client;

        public ApiClient(IApiConfiguration configuration)
        {
            _client = new HttpClient();
            _url = configuration.Url;
        }

        public async Task<Response<string>> AddCustomerAsync(Customer customer)
            => await SendAsync<Response<string>>(HttpMethod.Post, $"{_url}/Customers/addcustomer", customer);

        public async Task<Response<Customer>> GetCustomerByIdAsync(int id)
            => await SendAsync<Response<Customer>>(HttpMethod.Get, $"{_url}/Customers/getcustomer?id={id}");

        async Task<T> SendAsync<T>(HttpMethod method, string url, object content = null)
        {
            var message = new HttpRequestMessage(method, url);
            if (content != null)
            {
                message.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
            }
            message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await _client.SendAsync(message, HttpCompletionOption.ResponseContentRead);
            return JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync());
        }
    }
}
