﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Threading.Tasks;

namespace Otus.Teaching.WebApi.App
{
    public interface IClient
    {
        Task<Response<string>> AddCustomerAsync(Customer customer);
        Task<Response<Customer>> GetCustomerByIdAsync(int id);
    }
}
