﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.IO;

namespace Otus.Teaching.WebApi.App
{
    class Program
    {
        static void Main(string[] args)
        {
            var jsonConfig = Path.Combine(new DirectoryInfo(Environment.CurrentDirectory)
                .Parent.Parent.Parent.Parent.FullName, "Otus.Teaching.Concurrency.Import.Loader", "config.json");
            var builder = new ConfigurationBuilder().AddJsonFile(jsonConfig).Build();
            var settings = builder.GetSection("AppSettings").Get<AppConfiguration>();

            ApiClient client = new(settings);
            CustomerManager manager = new(client, new IntInputValidator());

            manager.Start();

            Console.ReadKey();
        }
    }
}
