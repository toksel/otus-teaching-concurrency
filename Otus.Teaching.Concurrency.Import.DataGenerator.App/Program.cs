﻿using System;
using System.IO;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public class Program
    {
        private static string _dataFileName;
        private static string _fileExtension;
        private static int _dataCount;
        readonly static string[] _availableFormats = { "xml", "csv" };

        public static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;

            Console.WriteLine($"Generating {_fileExtension} data...");

            var factory = new GeneratorFactory();
            var generator = factory.GetGenerator(_fileExtension, _dataFileName, _dataCount);

            generator.Generate();

            Console.WriteLine($"Generated xml data in {_dataFileName}...");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            _fileExtension = Path.GetExtension(args[0]).ToLower()[1..];
            if (args != null && args.Length > 0 && _availableFormats.Any(a => a == _fileExtension))
            {
                _dataFileName = args[0];
            }
            else
            {
                Console.WriteLine("Only csv and xml formats are available at this moment.");
                return false;
            }
            if (args.Length > 1)
            {
                if (!int.TryParse(args[1], out _dataCount))
                {
                    Console.WriteLine("Data count must be integer");
                    return false;
                }
            }
            return true;
        }
    }
}