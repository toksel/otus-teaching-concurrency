using Otus.Teaching.Concurrency.Import.Core.Data;
using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.App
{
    public class GeneratorFactory
    {
        Dictionary<string, Type> _generators;

        public GeneratorFactory()
        {
            _generators = new Dictionary<string, Type>()
            {
                { "xml", typeof(XmlGenerator) },
                { "csv", typeof(CsvGenerator) }
            };
        }
        public IDataGenerator GetGenerator(string type, string fileName, int dataCount)
            => (IDataGenerator)Activator.CreateInstance(_generators[type], fileName, dataCount);
    }
}