﻿using Otus.Teaching.Concurrency.Import.Core.Dto;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser: IDataParser<Customer>
    {
        public IList<Customer> Parse(string path)
        {
            var serializer = new XmlSerializer(typeof(CustomersList));
            using var fileStream = new FileStream(path, FileMode.Open);
            var ret = serializer.Deserialize(fileStream);
            return ((CustomersList)ret).Customers;
        }
    }
}