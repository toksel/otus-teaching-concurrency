﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class ParserFactory
    {
        Dictionary<string, Type> _parsers;

        public ParserFactory()
        {
            _parsers = new Dictionary<string, Type>()
            {
                { "xml", typeof(XmlParser) },
                { "csv", typeof(CsvParser) }
            };
        }
        public IDataParser<Customer> GetParser(string type)
            => (IDataParser<Customer>)Activator.CreateInstance(_parsers[type]);
    }
}
