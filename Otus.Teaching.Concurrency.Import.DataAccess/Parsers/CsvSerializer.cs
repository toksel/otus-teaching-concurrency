﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvSerializer<T> where T: class, new()
    {
        public char Delimiter { get; set; } = ',';
        public bool HasHeader { get; set; } = false;

        private FieldInfo[] _fields;

        public CsvSerializer()
        {
            var type = typeof(T);
            _fields = type.GetFields(BindingFlags.Instance  |
                                     BindingFlags.NonPublic |
                                     BindingFlags.Public);
        }

        string GetHeader()
            => string.Join(Delimiter.ToString(), typeof(T).GetProperties(BindingFlags.Instance |
                                                                         BindingFlags.NonPublic |
                                                                         BindingFlags.Public)
                                                           .Select(s => s.Name));

        public void Serialize(Stream stream, IList<T> data)
        {
            StringBuilder sb = new StringBuilder();
            if (HasHeader)
            {
                sb.AppendLine(GetHeader());
            }
            for (int i = 0; i < data.Count - 1; i++)
            {
                sb.AppendLine(SerializeItem(data[i]));
            }
            sb.Append(SerializeItem(data[data.Count - 1]));
            using var sw = new StreamWriter(stream);
            sw.Write(sb.ToString());
        }

        public void Serialize(Stream stream, T item)
            => Serialize(stream, new List<T>() { item });

        string SerializeItem(T item)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_fields.Select(s => (s.GetValue(item) ?? string.Empty).ToString())
                                 .Aggregate((a1, a2) => a1 + Delimiter + a2));
            return sb.ToString();
        }

        public IList<T> Deserialize(Stream stream)
        {
            string[] csvLines;
            using var sr = new StreamReader(stream);
            csvLines = sr.ReadToEnd().Split(Environment.NewLine);
            if (HasHeader)
            {
                csvLines = csvLines.Skip(1).ToArray();
            }
            var data = new List<T>();
            csvLines.ToList().ForEach(f => data.Add(DeserializeItem(f)));
            return data;
        }

        public T DeserializeItem(string csvLine)
        {
            string[] parameters = csvLine.Split(Delimiter);
            Type type = typeof(T);
            T obj = (T)Activator.CreateInstance(type);
            if (_fields.Length != parameters.Length)
            {
                throw new ArgumentException($"Faild to deserialize input string to object with type {type.Name}");
            }
            for (int i = 0; i < _fields.Length; i++)
            {
                _fields[i].SetValue(obj, Convert.ChangeType(parameters[i], _fields[i].FieldType));
            }
            return obj;
        }
    }
}
