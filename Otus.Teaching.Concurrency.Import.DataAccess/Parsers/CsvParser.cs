﻿using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using System.Collections.Generic;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    class CsvParser : IDataParser<Customer>
    {
        public IList<Customer> Parse(string path)
        {
            var serializer = new CsvSerializer<Customer>();
            using var fileStream = new FileStream(path, FileMode.Open);
            var ret = serializer.Deserialize(fileStream);
            return ret;
        }
    }
}
