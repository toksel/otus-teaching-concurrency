﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.DbContext;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class AppDbContext : DbContext, IDbContext
    {
        IDbConfiguration _configuration;

        public AppDbContext(IDbConfiguration configuration)
        {
            _configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_configuration.ConnectionString);
        }

        public DbSet<Customer> Customers { get; set; }

        public Response<string> AddCustomer(Customer customer)
        {
            var exists = Customers.Any(a => a.FullName == customer.FullName);
            if (!exists)
            {
                Customers.Add(customer);
                SaveChanges();
            }
            return new Response<string>
            {
                Status = exists ? HttpStatusCode.Conflict : HttpStatusCode.OK,
                Body = $"customer {customer.FullName} " + (exists ? "already exists" : "added successfully")
            };
        }

        public void AddCustomers(IList<Customer> customers)
        {
            Customers.AddRange(customers);
            SaveChanges();
        }
        public Response<Customer> GetCustomer(int id)
        {
            var ret = new Response<Customer>
            {
                Body = Customers.FirstOrDefault(f => f.Id == id)
            };
            ret.Status = ret.Body is null ? HttpStatusCode.NotFound : HttpStatusCode.OK;
            return ret;
        }
    }
}
