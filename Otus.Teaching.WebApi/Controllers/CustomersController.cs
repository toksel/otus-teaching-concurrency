﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.Core.DbContext;
using Otus.Teaching.Concurrency.Import.Core.Entities;

namespace Otus.Teaching.WebApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly IDbContext _context;
        public CustomersController(IDbContext context)
        {
            _context = context;
        }

        [HttpGet("getcustomer")]
        public Response<Customer> GetCustomer(int id)
        {
            var result = _context.GetCustomer(id);
            return result;
        }

        [HttpPost("addcustomer")]
        public Response<string> AddCustomer([FromBody] Customer customer)
            => _context.AddCustomer(customer);
    }
}
