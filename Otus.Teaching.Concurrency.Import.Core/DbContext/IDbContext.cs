﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.DbContext
{
    public interface IDbContext
    {
        DbSet<Customer> Customers { get; set; }
        public void AddCustomers(IList<Customer> customers);
        public Response<string> AddCustomer(Customer customer);
        public Response<Customer> GetCustomer(int id);
    }
}
