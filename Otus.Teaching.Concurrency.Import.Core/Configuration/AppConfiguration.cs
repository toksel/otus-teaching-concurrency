﻿using System.Linq;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core
{
    public class AppConfiguration : IAppConfiguration, IDbConfiguration, IApiConfiguration
    {
        public string ConnectionString { get; set; }
        public string FilePath { get; set; }
        public GenerateMode GenerateMode { get; set; }
        public LoaderMode LoaderMode { get; set; }
        public int EntitiesCount { get; set; }
        public int ThreadsCount { get; set; }
        public int RetryCount { get; set; }
        public string Url { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine("You're using config.json with parameters:");
            GetType().GetProperties(BindingFlags.Instance |
                                    BindingFlags.NonPublic |
                                    BindingFlags.Public)
                     .ToList()
                     .ForEach(f => sb.AppendLine($"{f.Name}: {f.GetValue(this)}"));
            return sb.ToString();
        }
    }
}
