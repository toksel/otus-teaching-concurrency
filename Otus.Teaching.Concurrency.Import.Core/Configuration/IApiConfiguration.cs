﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IApiConfiguration
    {
        public string Url { get; set; }
    }
}
