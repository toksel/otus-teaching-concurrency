﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IDbConfiguration
    {
        public string ConnectionString { get; set; }
    }
}
