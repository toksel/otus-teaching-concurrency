﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public interface IAppConfiguration
    {
        public string ConnectionString { get; set; }
        public string FilePath { get; set; }
        public GenerateMode GenerateMode { get; set; }
        public LoaderMode LoaderMode { get; set; }
        public int EntitiesCount { get; set; }
        public int ThreadsCount { get; set; }
        public int RetryCount { get; set; }
    }
}
