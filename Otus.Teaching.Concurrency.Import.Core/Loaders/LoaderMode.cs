﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum LoaderMode
    {
        Unknown,
        Separete,
        ThreadPool,
        Single,
    }
}
