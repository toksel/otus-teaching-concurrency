﻿namespace Otus.Teaching.Concurrency.Import.Core
{
    public enum GenerateMode
    {
        Unknown,
        Method,
        Process
    }
}
