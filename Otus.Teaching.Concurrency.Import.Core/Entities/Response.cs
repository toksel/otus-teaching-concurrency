﻿using System.Linq;
using System.Net;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.Entities
{
    public class Response<T>
    {
        public HttpStatusCode Status { get; set; }
        public T Body { get; set; }
        public override string ToString()
        {
            int bodyLength = Body is null ? 0 : Body.ToString().Length + 6 ;
            int statusLength = Status.ToString().Length + 6;
            int messageHeaderLenthg = "Web Api response received:".Length;
            var beautify = new string('*', new int[3] { bodyLength, statusLength, messageHeaderLenthg }.Max());
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(beautify);
            sb.AppendLine("Web Api response received:");
            sb.AppendLine($"Code: {Status}");
            sb.AppendLine($"Body: {Body}");
            sb.AppendLine(beautify);
            sb.AppendLine();
            return sb.ToString();
        }

    }
}
