using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.Concurrency.Import.Core.Entities
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public override string ToString()
            => $"Id: {Id}, Name: {FullName}, Email: {Email}, Phone: {Phone}";
    }
}